*&---------------------------------------------------------------------*
*&  Include           ZP2P_R_SALES_SCHD_CLOSURE_F01
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Form  GET_SSA_SELECTION_DATA
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM get_ssa_selection_data.

*get CCR relevent data from the custom CCR table based on selection screen parameters
  SELECT erdat
         zordno
         febeln
         matnr
         werks
         menge
         lebeln
         lvbeln
         fvbeln
         ncind
    FROM zp2p_ccr_scpoedm
    INTO TABLE gt_ccr_date_spc
    WHERE erdat = p_date
    AND   zordno IN s_ord_no
    AND   febeln IN s_sub_po
    AND   matnr  IN s_matnr.

  IF sy-subrc = 0.

    SELECT erdat
           zordno
           febeln
           matnr
           werks
           menge
           lebeln
           lvbeln
           fvbeln
           ncind
    FROM zp2p_ccr_scpoedm
    INTO TABLE gt_zp2p_ccr_scpoedm
    FOR ALL ENTRIES IN gt_ccr_date_spc
    WHERE zordno = gt_ccr_date_spc-zordno
    AND   febeln = gt_ccr_date_spc-febeln.

    IF sy-subrc = 0.
*sort the CCR table data on the basis of Subcon PO and date
      SORT gt_zp2p_ccr_scpoedm BY febeln  DESCENDING erdat.

*get the part type from the plant-material table
      SELECT matnr
             werks
             zzccrpartype
        FROM marc
        INTO TABLE gt_marc
        FOR ALL ENTRIES IN gt_zp2p_ccr_scpoedm
        WHERE matnr = gt_zp2p_ccr_scpoedm-matnr
        AND werks = gt_zp2p_ccr_scpoedm-werks
        AND zzccrpartype EQ 'ZCCC'.

*procesed only if the part type is equal to 'ZCCC'
      IF sy-subrc = 0.
        SORT gt_marc BY  matnr werks.

*get data from the Sales Document: Item Data table
        SELECT vbeln
               posnr
               matnr
               abgru
               werks
          FROM vbap
          INTO TABLE gt_vbap
          FOR ALL ENTRIES IN gt_marc
          WHERE matnr = gt_marc-matnr
          AND werks = gt_marc-werks.
        IF sy-subrc = 0.
          SORT gt_vbap BY matnr abgru werks.

*get the Sales Org from the ZP2P_PARAMS table
          SELECT zvalue1
            FROM zp2p_param
            INTO TABLE gt_vkorg
            WHERE zparm1 = gc_vkorg_name.
          IF sy-subrc = 0.
            LOOP AT gt_vkorg INTO gs_vkorg.
              rt_vkorg-sign = 'I'.
              rt_vkorg-option = 'EQ'.
              rt_vkorg-low = gs_vkorg-vkorg.
              APPEND rt_vkorg.
            ENDLOOP.
          ENDIF.

*get the document type from ZP2P_PARAMS table
          SELECT zvalue1
            FROM zp2p_param
            INTO TABLE gt_auart
            WHERE zparm1 = gc_auart_name.
          IF sy-subrc = 0.
            LOOP AT gt_auart INTO gs_auart.
              rt_auart-sign = 'I'.
              rt_auart-option = 'EQ'.
              rt_auart-low = gs_auart-auart.
              APPEND rt_auart.
            ENDLOOP.
          ENDIF.

*get data from Sales Document: Header Data table
          IF gt_vkorg[] IS NOT INITIAL AND gt_auart[] IS NOT INITIAL.

            SELECT vbeln
                   guebg
                   gueen
            FROM vbak
            INTO TABLE gt_vbak
            FOR ALL ENTRIES IN gt_vbap
            WHERE vbeln = gt_vbap-vbeln
            AND (
                ( guebg LE p_date AND gueen GT p_date )
             OR ( guebg LT p_date AND gueen GE p_date )
                )
            AND vkorg IN rt_vkorg
            AND auart IN rt_auart.
            IF sy-subrc = 0.
              SORT gt_vbak BY vbeln guebg.
            ENDIF.
          ENDIF.

*get Sales Document: Schedule Line Data
          SELECT   vbeln
                   posnr
                   edatu
                   etenr
                   wmeng
              FROM vbep
              INTO TABLE gt_vbep
              FOR ALL ENTRIES IN gt_vbap
              WHERE vbeln = gt_vbap-vbeln
              AND posnr = gt_vbap-posnr
              AND abart = '1'.
          IF sy-subrc = 0.
            SORT gt_vbep BY vbeln posnr edatu.
          ENDIF.
        ENDIF.
      ENDIF.
    ENDIF.
  ELSE.
    IF sy-batch IS INITIAL.
      MESSAGE text-001 TYPE gc_e.
    ELSE.
      MESSAGE text-001 TYPE gc_s DISPLAY LIKE gc_e.
    ENDIF.
  ENDIF.
ENDFORM.                    " GET_CCR_DATA
*&---------------------------------------------------------------------*
*&      Form  GET_COMP_PART_AND_QTY
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM get_comp_part_and_qty .

  DATA : lv_pos TYPE i,
         lv_qty TYPE wmeng.

  CONSTANTS : lc_n          TYPE zzccrind VALUE 'N',
              lc_c          TYPE zzccrind VALUE 'C',
              lc_d          TYPE zzccrind VALUE 'D',
              lc_new_subcon TYPE char20   VALUE 'NEW_SUBCON',
              lc_added_part TYPE char20   VALUE 'PART_ADDED',
              lc_del_part   TYPE char20   VALUE 'PART_DEL',
              lc_qty_dec    TYPE char20   VALUE 'QTY_DEC',
              lc_qty_inc    TYPE char20   VALUE 'QTY_INC',
              lc_del_subcon TYPE char20   VALUE 'DEL_SUBCON'.


*get the position of each record based on the date, I/O number and Subcon PO
  LOOP AT gt_zp2p_ccr_scpoedm INTO gs_zp2p_ccr_scpoedm.
    gs_ccr_data-erdat  = gs_zp2p_ccr_scpoedm-erdat.
    gs_ccr_data-zordno = gs_zp2p_ccr_scpoedm-zordno.
    gs_ccr_data-febeln = gs_zp2p_ccr_scpoedm-febeln.
    gs_ccr_data-matnr  = gs_zp2p_ccr_scpoedm-matnr.
    gs_ccr_data-werks  = gs_zp2p_ccr_scpoedm-werks.
    gs_ccr_data-menge  = gs_zp2p_ccr_scpoedm-menge.
    gs_ccr_data-lebeln = gs_zp2p_ccr_scpoedm-lebeln.
    gs_ccr_data-lvbeln = gs_zp2p_ccr_scpoedm-lvbeln.
    gs_ccr_data-fvbeln = gs_zp2p_ccr_scpoedm-fvbeln.
    gs_ccr_data-ncind  = gs_zp2p_ccr_scpoedm-ncind.
    gs_ccr_data-pos    = lv_pos.
    APPEND gs_ccr_data TO gt_ccr_data.
    CLEAR gs_ccr_data.

    AT END OF erdat.
      lv_pos   = lv_pos + 1.
    ENDAT.
  ENDLOOP.



  LOOP AT gt_ccr_date_spc INTO gs_ccr_date_spc.

*For record record of Sales Document: Item Data table
    READ TABLE gt_marc
    INTO gs_marc
    WITH KEY matnr = gs_ccr_date_spc-matnr
             werks = gs_ccr_date_spc-werks
             BINARY SEARCH.
    IF sy-subrc = 0.

      CASE gs_ccr_date_spc-ncind.

        WHEN lc_n.                                                "new indicator

          gs_qty_data-matnr = gs_ccr_date_spc-matnr.              "Material number
          gs_qty_data-werks = gs_ccr_date_spc-werks.              "Plant
          gs_qty_data-ncind = lc_n.                               "Indicator
          gs_qty_data-desc  = lc_new_subcon.                      "description
          gs_qty_data-ref_qty = 0 - gs_ccr_date_spc-menge.        "reference quantity
          APPEND gs_qty_data TO gt_qty_data.
          CLEAR gs_qty_data.

        WHEN lc_c.                                                "change indicator

*get the input date CCR record
          READ TABLE gt_ccr_data
          INTO  gs_ccr_data_rec
          WITH KEY erdat = p_date
                   zordno = gs_ccr_date_spc-zordno
                   febeln = gs_ccr_date_spc-febeln
                   matnr  = gs_ccr_date_spc-matnr.
          IF sy-subrc = 0.

*Calculate position for fetching the previous day record
            gv_pos = gs_ccr_data_rec-pos - 1.

*get the previous day record based on calculated position
            READ TABLE gt_ccr_data
            INTO gs_ccr_data_prev
            WITH KEY zordno = gs_ccr_date_spc-zordno
                     febeln = gs_ccr_date_spc-febeln
                     matnr  = gs_ccr_data_rec-matnr
                     pos    = gv_pos.
            IF sy-subrc <> 0.                                      "if part component not found

              gs_qty_data-matnr = gs_ccr_data_rec-matnr.           "Material number
              gs_qty_data-werks = gs_ccr_data_rec-werks.           "Plant
              gs_qty_data-ncind = lc_c.                            "indicator
              gs_qty_data-desc  = lc_added_part.                   "description
              gs_qty_data-ref_qty = 0 - gs_ccr_data_rec-menge.     "reference quantity
              APPEND gs_qty_data TO gt_qty_data.
              CLEAR gs_qty_data.

            ELSE.                                                  "if part component found
*Input date record quantity is greater than previous date record quantity
              IF gs_ccr_data_rec-menge > gs_ccr_data_prev-menge.

                gs_qty_data-matnr = gs_ccr_data_rec-matnr.         "Material number
                gs_qty_data-werks = gs_ccr_data_rec-werks.         "Plant
                gs_qty_data-ncind = lc_c.                          "indicator
                gs_qty_data-desc  = lc_qty_inc.                    "description
                gs_qty_data-ref_qty = 0 - ( gs_ccr_data_rec-menge - gs_ccr_data_prev-menge ). "reference quantity
                APPEND gs_qty_data TO gt_qty_data.
                CLEAR gs_qty_data.

*Input date record quantity is less than previous date record quantity
              ELSEIF   gs_ccr_data_rec-menge < gs_ccr_data_prev-menge.

                gs_qty_data-matnr = gs_ccr_data_rec-matnr.         "Material number
                gs_qty_data-werks = gs_ccr_data_rec-werks.         "Plant
                gs_qty_data-ncind = lc_c.                          "indicator
                gs_qty_data-desc  = lc_qty_dec.                    "description
                gs_qty_data-ref_qty = ( gs_ccr_data_prev-menge - gs_ccr_data_rec-menge ). "reference quantity
                APPEND gs_qty_data TO gt_qty_data.
                CLEAR gs_qty_data.

              ENDIF.
            ENDIF.

            CLEAR gs_ccr_data_prev.
            CLEAR gs_ccr_data_rec.
          ENDIF.

          AT END OF febeln.
*logic to check if part component is deleted
            LOOP AT gt_ccr_data  INTO  gs_ccr_data_prev
                                 WHERE zordno = gs_ccr_date_spc-zordno
                                 AND   febeln = gs_ccr_date_spc-febeln
                                 AND   pos    = gv_pos.

*read input date record
              READ TABLE gt_ccr_data
              INTO gs_ccr_data_rec
              WITH KEY erdat = p_date
                       zordno = gs_ccr_date_spc-zordno
                       febeln = gs_ccr_date_spc-febeln
                       matnr  = gs_ccr_data_prev-matnr.
              IF sy-subrc = 0.
                CONTINUE.
              ELSE.                                              "if not found

                gs_qty_data-matnr = gs_ccr_data_prev-matnr.      "Material number
                gs_qty_data-werks = gs_ccr_data_prev-werks.      "Plant
                gs_qty_data-ncind = lc_c.                        "indicator
                gs_qty_data-desc  = lc_del_part.                 "description
                gs_qty_data-ref_qty = gs_ccr_data_prev-menge.    "reference quantity
                APPEND gs_qty_data TO gt_qty_data.
                CLEAR gs_qty_data.

              ENDIF.

              CLEAR gs_ccr_data_prev.
              CLEAR gs_ccr_data_rec.
            ENDLOOP.
          ENDAT.

        WHEN lc_d.                                            "deletion indicator
          gs_qty_data-matnr = gs_ccr_date_spc-matnr.          "Material number
          gs_qty_data-werks = gs_ccr_date_spc-werks.          "Plant
          gs_qty_data-ncind = lc_d.                           "indicator
          gs_qty_data-desc  = lc_del_subcon.                  "description
          gs_qty_data-ref_qty = gs_ccr_date_spc-menge.        "reference quantity
          APPEND gs_qty_data TO gt_qty_data.
          CLEAR gs_qty_data.

      ENDCASE.

    ENDIF.
    CLEAR gs_ccr_date_spc.
    CLEAR gs_marc.
  ENDLOOP.



  SORT gt_qty_data BY matnr werks.

  LOOP AT gt_qty_data INTO gs_qty_data.

    lv_qty = lv_qty + gs_qty_data-ref_qty.                  "Quantity to be added or deleted

    AT END OF werks.

*For record record of Sales Document: Item Data table
      READ TABLE gt_vbap
      INTO gs_vbap
      WITH KEY matnr = gs_qty_data-matnr
               werks = gs_qty_data-werks
               BINARY SEARCH.
      IF sy-subrc = 0.

        IF gs_vbap-abgru = space.

*get the data from Sales Document: Header Data table based on Item Data table
          READ TABLE gt_vbak
                 INTO gs_vbak
                 WITH KEY vbeln = gs_vbap-vbeln
                 BINARY SEARCH.
          IF sy-subrc  = 0.

*get corresponding data from Sales Document: Schedule Line Data
            READ TABLE  gt_vbep
            INTO gs_vbep
            WITH KEY vbeln = gs_vbap-vbeln
                     posnr = gs_vbap-posnr.

            IF sy-subrc = 0.

              gs_process-matnr = gs_qty_data-matnr.                 "Part number
              gs_process-vbeln = gs_vbep-vbeln.                     "SSA Number
              gs_process-posnr = gs_vbep-posnr.                     "SSA item
              gs_process-etenr = gs_vbep-etenr.                     "SSA line number
              gs_process-prev_qty = gs_vbep-wmeng.                  "initial SSA order quantity
              gs_process-req_qty = gs_vbep-wmeng + lv_qty.          "order quantity to be changed
              gs_process-chg_qty = gs_process-req_qty - gs_process-prev_qty . "SSA Schedule Quantity Change

              PERFORM update_of_ssa USING gs_process.               "UpdatE SSA quantity in the forcast tab

            ELSE.
              gs_process-matnr  = gs_qty_data-matnr.                "Part number
              gs_process-vbeln  = gs_vbap-vbeln.                    "SSA Number
              gs_process-posnr  = gs_vbap-posnr.                    "SSA item
              gs_process-status = text-014.                         "Insufficient Sales Schedule line quantity
              gs_process-adj_qty = lv_qty.                          "SSA quantity not adjusted
              APPEND gs_process TO gt_process.
              CLEAR gs_process.
            ENDIF.

          ELSE.
            gs_process-matnr  = gs_qty_data-matnr.                  "Part number
            gs_process-vbeln  = gs_vbap-vbeln.                      "SSA Number
            gs_process-posnr  = gs_vbap-posnr.                      "SSA item
            gs_process-status = text-021.                           "SSA not found
            gs_process-adj_qty = lv_qty.                            "SSA quantity not adjusted
            APPEND gs_process TO gt_process.
            CLEAR gs_process.
          ENDIF.

        ELSE.
          gs_process-matnr  = gs_qty_data-matnr.                    "Part number
          gs_process-vbeln  = gs_vbap-vbeln.                        "SSA Number
          gs_process-posnr  = gs_vbap-posnr.                        "SSA item
          gs_process-status = text-020.                             "No Change in Sales Schedule line, Sales Scheduling Agreement not available
          gs_process-adj_qty = lv_qty.                              "SSA quantity not adjusted
          APPEND gs_process TO gt_process.
          CLEAR gs_process.
        ENDIF.

      ELSE.
        gs_process-matnr  = gs_qty_data-matnr.                      "Part number
        gs_process-status = text-021.                               "SSA not found
        gs_process-adj_qty = lv_qty.                                "SSA quantity not adjusted
        APPEND gs_process TO gt_process.
        CLEAR gs_process.
      ENDIF.

      CLEAR lv_qty.
      CLEAR gs_vbep.
      CLEAR gs_vbak.
      CLEAR gs_vbap.
    ENDAT.

    CLEAR gs_qty_data.
  ENDLOOP.

ENDFORM.                    " GET_COMP_PART_AND_QTY
*&---------------------------------------------------------------------*
*&      Form  UPDATE_OF_SSA
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM update_of_ssa USING gs_process TYPE gty_process.

  DATA:      ls_salesdocument    TYPE bapivbeln-vbeln,
             ls_order_header_inx TYPE bapisdh1x,
             ls_order_item_in    TYPE bapisditm,
             ls_order_item_inx   TYPE bapisditmx,
             ls_schedule_lines   TYPE bapischdl,
             ls_schedule_linesx  TYPE bapischdlx,
             ls_return           TYPE bapiret2,
             lt_order_item_in    TYPE STANDARD TABLE OF bapisditm,
             lt_order_item_inx   TYPE STANDARD TABLE OF bapisditmx,
             lt_schedule_lines   TYPE STANDARD TABLE OF bapischdl,
             lt_schedule_linesx  TYPE STANDARD TABLE OF bapischdlx,
             lt_return           TYPE STANDARD TABLE OF bapiret2,

             lv_balc_qty         TYPE wmeng,
             lv_etenr            TYPE etenr.

  CONSTANTS : lc_u               TYPE char1  VALUE 'U',
              lc_e               TYPE char1  VALUE 'E',
              lc_s               TYPE char1  VALUE 'S',
              lc_x               TYPE char1  VALUE 'X'.

  IF  gs_process-req_qty LT 0.

    lv_balc_qty = 0 - gs_process-req_qty.

    DELETE gt_vbep WHERE vbeln = gs_process-vbeln
                   AND   posnr = gs_process-posnr
                   AND   etenr = gs_process-etenr.

    gs_process-req_qty = 0.

    gs_process-chg_qty = gs_process-req_qty - gs_process-prev_qty .             "SSA Order quantity change

    gs_process1-matnr = gs_process-matnr.                                       "Part number
    gs_process1-vbeln = gs_process-vbeln.                                       "SSA Number
    gs_process1-posnr = gs_process-posnr.                                       "SSA item

    PERFORM update_of_ssa USING gs_process.                                     "Update SSA Order quantity

    CLEAR gs_vbep.
    READ TABLE gt_vbep
    INTO gs_vbep
    WITH KEY vbeln = gs_process1-vbeln
             posnr = gs_process1-posnr.

    IF sy-subrc <> 0.

      DELETE gt_vbak WHERE vbeln = gs_process1-vbeln.

      READ TABLE gt_vbak INTO gs_vbak INDEX 1.

      IF sy-subrc <> 0.

        gs_process-status  = text-014.                                            "insufficient order quantity
        gs_process-adj_qty = gs_process-req_qty.                                  "Quantity not adjusted
        MODIFY gt_process
        FROM gs_process
        TRANSPORTING status adj_qty
        WHERE vbeln = gs_process-vbeln
        AND posnr = gs_process-posnr.

      ELSE.

        gs_process1-etenr = gs_vbep-etenr.                                     "SSA line number
        gs_process1-req_qty = gs_vbep-wmeng - lv_balc_qty.                     "SSA updated order quantity
        gs_process1-chg_qty = gs_process1-req_qty - gs_vbep-wmeng.             "SSA Schedule Quantity Change
        PERFORM update_of_ssa USING gs_process1.                               "Update SSA Order quantity

      ENDIF.

    ELSE.
      gs_process1-etenr = gs_vbep-etenr.                                     "SSA line number
      gs_process1-req_qty = gs_vbep-wmeng - lv_balc_qty.                     "SSA updated order quantity
      gs_process1-chg_qty = gs_process1-req_qty - gs_vbep-wmeng.             "SSA Schedule Quantity Change
      PERFORM update_of_ssa USING gs_process1.                               "Update SSA Order quantity

    ENDIF.

  ELSE.
    ls_salesdocument = gs_process-vbeln.
    ls_order_header_inx-updateflag = lc_u.

    ls_order_item_in-itm_number = gs_process-posnr.
    APPEND ls_order_item_in TO lt_order_item_in.

    ls_order_item_inx-itm_number = gs_process-posnr.
    ls_order_item_inx-updateflag = lc_u.
    APPEND ls_order_item_inx TO lt_order_item_inx.

    ls_schedule_lines-itm_number = gs_process-posnr.
    ls_schedule_lines-sched_line = gs_process-etenr.
    ls_schedule_lines-req_qty = gs_process-req_qty.
    ls_schedule_lines-rel_type = '1'.
    APPEND ls_schedule_lines TO lt_schedule_lines.

    ls_schedule_linesx-itm_number = gs_process-posnr.
    ls_schedule_linesx-sched_line = gs_process-etenr.
    ls_schedule_linesx-req_qty = lc_x.
    ls_schedule_linesx-updateflag = lc_u.
    APPEND ls_schedule_linesx TO lt_schedule_linesx.

*Call of BAPI to change the SSA quantity in the forecast Tab
    CALL FUNCTION 'BAPI_SALESORDER_CHANGE'
      EXPORTING
        salesdocument    = ls_salesdocument
        order_header_inx = ls_order_header_inx
      TABLES
        return           = lt_return
        order_item_in    = lt_order_item_in
        order_item_inx   = lt_order_item_inx
        schedule_lines   = lt_schedule_lines
        schedule_linesx  = lt_schedule_linesx.

    READ TABLE lt_return INTO ls_return WITH KEY type = lc_e.

    IF sy-subrc <> 0.

      CALL FUNCTION 'BAPI_TRANSACTION_COMMIT'
        EXPORTING
          wait = lc_x.

    ELSE.

      LOOP AT lt_return INTO ls_return.

        gs_process-status = ls_return-message.                      "return error message

        CLEAR ls_return.
      ENDLOOP.

    ENDIF.

*Final status log table
    APPEND gs_process TO gt_process.

    CLEAR gs_process.
    CLEAR ls_return.
    REFRESH lt_return.
  ENDIF.
ENDFORM.                    " UPDATE_OF_SSA
*&---------------------------------------------------------------------*
*&      Form  DISPLAY_LOG
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM display_log USING gt_process TYPE gtt_process.
  REFRESH it_fieldcat.
  PERFORM alv_fieldcat USING 1 1  text-003  text-002 text-007   15 .  "SSA Part Number
  PERFORM alv_fieldcat USING 1 2  text-004  text-002 text-008   25 .  "SSA Number
  PERFORM alv_fieldcat USING 1 3  text-005  text-002 text-009   25 .  "SSA Item Number
  PERFORM alv_fieldcat USING 1 4  text-016  text-002 text-017   25 .  "SSA Schedule line Number
  PERFORM alv_fieldcat USING 1 5  text-012  text-002 text-013   25 .  "SSA Schedule Quantity Change
  PERFORM alv_fieldcat USING 1 6  text-006  text-002 text-010   25 .  "Updated SSA Quantity
  PERFORM alv_fieldcat USING 1 7  text-018  text-002 text-019   25 .  "Status Log
  PERFORM alv_fieldcat USING 1 8  text-022  text-002 text-023   25 .  "Quantity Not Adjusted

  PERFORM alv_layout USING text-011 gc_x.

  PERFORM display_alv.
ENDFORM.                    " DISPLAY_LOG
*&---------------------------------------------------------------------*
*&      Form  ALV_102_FIELDCAT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM alv_fieldcat USING p_row p_col p_field p_table p_text p_len.

  CLEAR ty_fieldcat.
  ty_fieldcat-row_pos   = p_row.        "row number
  ty_fieldcat-col_pos   = p_col.        "column number
  ty_fieldcat-fieldname = p_field.      "fieldname
  ty_fieldcat-tabname   = p_table.      "table name
  ty_fieldcat-seltext_l = p_text.       "display text
  ty_fieldcat-outputlen = p_len.        "column length

  APPEND ty_fieldcat TO it_fieldcat.
  CLEAR ty_fieldcat.

ENDFORM.                    " ALV_102_FIELDCAT
*&---------------------------------------------------------------------*
*&      Form  ALV_102_LAYOUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM alv_layout USING p_title p_opt.
  CLEAR ty_lay1.
  ty_lay1-detail_titlebar    = p_title.         "title
  ty_lay1-zebra              = gc_x.            "zebra pattern
  ty_lay1-colwidth_optimize  = p_opt.           "optimize col width
ENDFORM.                    " ALV_102_LAYOUT
*&---------------------------------------------------------------------*
*&      Form  DISPLAY_ALV
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM display_alv .

  DATA  ls_print    TYPE slis_print_alv. ##needed

  IF sy-batch IS NOT INITIAL.
    ls_print-print = gc_x.                            "For background generate ALV output in Spool
    ls_print-no_print_listinfos = gc_x.
  ENDIF.

  CALL FUNCTION 'REUSE_ALV_GRID_DISPLAY'
    EXPORTING
      is_layout     = ty_lay1
      it_fieldcat   = it_fieldcat
      is_print      = ls_print
    TABLES
      t_outtab      = gt_process
    EXCEPTIONS
      program_error = 1
      OTHERS        = 2.

  IF sy-subrc <> 0.
    MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
  ENDIF.

ENDFORM.                    " DISPLAY_ALV
*&---------------------------------------------------------------------*
*&      Form  AUTHORITY_CHECK
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM authority_check .
  LOOP AT s_plant.
    AUTHORITY-CHECK OBJECT 'M_EINF_WRK'
    ID 'WERKS' FIELD s_plant-low
    ID 'ACTVT' FIELD '02'.
    IF sy-subrc <> 0.
      MESSAGE e000 WITH s_plant-low.
    ENDIF.
  ENDLOOP.

  LOOP AT s_sa_org.
    AUTHORITY-CHECK OBJECT 'V_VBKA_VKO'
             ID 'VKORG' FIELD s_sa_org-low
             ID 'VTWEG' DUMMY
             ID 'SPART' DUMMY
             ID 'VKBUR' DUMMY
             ID 'VKGRP' DUMMY
             ID 'KTAAR' DUMMY
             ID 'ACTVT' FIELD '02'.
    IF sy-subrc <> 0.
      MESSAGE e001 WITH s_sa_org-low.
    ENDIF.
  ENDLOOP.

  LOOP AT s_doc_ty.
    AUTHORITY-CHECK OBJECT 'V_VBAK_AAT'
         ID 'AUART' FIELD s_doc_ty-low
         ID 'ACTVT' FIELD '02'.
    IF sy-subrc <> 0.
      MESSAGE e002 WITH s_doc_ty-low.
    ENDIF.
  ENDLOOP.
ENDFORM.                    " AUTHORITY_CHECK